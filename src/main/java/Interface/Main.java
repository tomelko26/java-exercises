package Interface;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {


        System.out.println(InterfaceName.PI);

        System.out.println("--------------------------------------------------------");

        int[] tab = new int[5];

        tab[0] = 6;
        tab[1] = 4;
        tab[2] = 2;
        tab[3] = 11;
        tab[4] = 5;

//        Arrays.sort(tab);
        System.out.println(tab[0]);

        System.out.println("--------------------------------------------------------");


        Employee[] employees = new Employee[6];
        employees[0] = new Employee(10000);
        employees[1] = new Employee(1000);
        employees[2] = new Employee(4000);
        employees[3] = new Employee(9000);
        employees[4] = new Employee(2000);
        employees[5] = new Employee(7000);



        for (Employee e : employees){
            System.out.println(e.getSalary());
        }
        System.out.println("--..--..--..--..--..--..--..-BEFORE SORT-..--..--..--..--..--..--..--");


        System.out.println(employees[0].compareTo(employees[1]));
        System.out.println("--..--..--..--..--..--..--..--..--..--..--..--..--..--..--");


        Arrays.sort(employees);

        for (Employee e : employees){
            System.out.println(e.getSalary());
        }
        System.out.println("--..--..--..--..--..--..--..--..-AFTER SORT-..--..--..--..--..--..--");


        Arrays.sort(employees, Collections.<Employee>reverseOrder());

        for(Employee e : employees){
            System.out.println(e.getSalary());
        }
        System.out.printf("--..--..--..--..--..--..--..--..-AFTER SORT IN REVERSE ORDER-..--..--..--..--..--..--");
    }


}

