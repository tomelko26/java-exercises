package Interface;

public interface InterfaceName {

    double PI = 3.14; //public static void

    void something(); // public abstract
                        // it has to be @Override in class which ll be implements this
                        // interface
}
