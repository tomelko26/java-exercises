package Interface;

public class Employee implements InterfaceName, SomeThings, Comparable {

    @Override
    public void something() {
        throw new UnsupportedOperationException("Not supported yet");
//        // to change body of generated methods, choose
    }

    private double salary;

    public double getSalary(){
        return this.salary;
    }

    public Employee(double salary) {
        this.salary = salary;
    }

    @Override
    public int compareTo(Object o) {

        Employee sentBy =(Employee)o;

        if (this.salary < sentBy.salary){
            return -1;
        } else if (this.salary > sentBy.salary){
            return 1;
        } else {
            return 0;
        }
    }
}
