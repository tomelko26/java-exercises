package ReferenceVariables;

public class References {

    public static void main(String[] args){

        int a = 53;
        int b = a;

        b = 30;

        System.out.println(a);

        Test x = new Test(); /* zmienna referencyjna
                                pod x nie znajduje się żadna wartość zwykła tylko adres
                              */
        Test y = x;

        y.a = 40;

        System.out.println(x.a);
        System.out.println(y.a);

        String name = "Tomasz";
        String name2 = new String();
        name2 = "Nati";

        System.out.println(name);


    }

}
