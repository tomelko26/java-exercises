package ReferenceVariables;

public class TransferOfReferencesValues {
    public static void main(String[] args) {

        int a = 5;

        TransferOfReferencesValueTest x = new TransferOfReferencesValueTest();
        x.changeValue(a);
        System.out.println(a);

        Foo foo = new Foo();

        x.changeV(foo);

        System.out.println(foo.y);
    }
}

class Foo {
    int y = 20;
}