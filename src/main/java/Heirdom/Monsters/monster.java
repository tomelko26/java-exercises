package Heirdom.Monsters;

public abstract class monster {

    private double movSpeed;
    private double vitality;

    public double getMovSpeed() {
        return movSpeed;
    }

    public double getVitality() {
        return vitality;
    }

    void attack(){
        System.out.println("Monster's attacking");
    }

    protected abstract void description();

    public monster() {
        System.out.println("Monster's default constructor");
    }

    public monster(double movSpeed, double vitality) {
        this.movSpeed = movSpeed;
        this.vitality = vitality;
        System.out.println("Monster's non-default constructor");
    }
}
