package Heirdom.Monsters;

public class skeleton extends monster {

    String weponType;

    public skeleton() {
        System.out.println("Skeleton's defoult constructor");
    }

    public skeleton(double movSpeed, double vitality){
        super(movSpeed, vitality);
    }

    public skeleton(double movSpeed, double vitality, String weponType){
        super(movSpeed, vitality);
        this.weponType = weponType;
        System.out.println("Skeleton's non-defoult constructor");
    }

    @Override
    public void attack(){
        System.out.println("skeleton' s attacking");
    }

    protected void description() {

    }
}
