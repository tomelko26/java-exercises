package pkginterfaceof;

public class Main {

    public static void main(String[] args) {

        Person[] person = new Person[4];
        person[0] = new Employee(" Tomek ", " Rojek ", 4000);
        person[1] = new Student(" Natka ", " Kotka ");
        person[2] = new Employee(" Juzio ", " ZkrzywąBuzią ", 3000);

        person[0].Description();
        person[1].Description();

        for (Person person1 : person){       /* ALTERNATIVE  */
//                (int i = 0; i < person.length; i++){
//                    if (person[i].instanceof Person){
//                        person[i].Description();
//                    }
            if (person1 instanceof Person){
                person1.Description();
            } else {
                break;
            }
        }

        for (Person person1 : person){
            if (person1 instanceof Employee){
                ((Employee)person1).work();
            } else {
                break;
            }
        }

    }
}
