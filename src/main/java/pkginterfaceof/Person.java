package pkginterfaceof;


import javax.naming.ldap.StartTlsRequest;

abstract class Person {
    String name;
    String surname;


    Person(String name, String surname){
        this.name = name;
        this.surname = surname;
    }

    abstract void Description ();
}
