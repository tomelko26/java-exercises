package pkginterfaceof;

public class Student extends Person {

    public Student(String name, String surname) {
        super(name, surname);
    }

    void Description() {
        System.out.println(name + surname);
    }
}
