package pkginterfaceof;

public class Employee extends Person {

    double reward;

    public Employee(String name, String surname, double reward) {
        super(name, surname);
        this.reward = reward;
    }


    void Description() {
        System.out.println(name + surname + reward);
    }

    void work() {
        System.out.println("call the work class");
    }
}
