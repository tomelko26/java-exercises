package ObjectProg;

public class ObjectProgramming {
    public static void main(String[] args) {



        Pkt p1= new Pkt();
        Pkt p2= new Pkt(10,155);
        Pkt p3= new Pkt(10, 45);


        System.out.println("p.x " + p1.x);
        System.out.println("p.y " + p1.y);

        System.out.println("p.x " + p2.x);
        System.out.println("p.y " + p2.y);

        System.out.println("p.x " + p3.x);
        System.out.println("p.y " + p3.y);
    }
}

class Pkt {

    Pkt(){ //po wywołaniu konstruktora
        System.out.println("wywołano konstruktor domyślny");

//        x = 20;
//        y = 20;
    }

    Pkt(int first, int second){
        System.out.println("wywołano konstruktor with two parameters");

        x= first;
        y=second;
    }

    int x;
    int y;


}
