package ObjectClassCompareOfObjects;

public class Pkt {

    private int x;
    private int y;

    @Override
    public boolean equals (Object o) {

        if (this == o ){
            return true;
        }

        if (o == null){
            return false;
        }

        if (this.getClass() != o.getClass()){
            return false;
        }

        Pkt sented = (Pkt)o;

        return (this.x == sented.x && this.y == sented.y);
    }

    @Override
    public String toString() {
        return " " + getX() + " " + getY();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Pkt(int x, int y) {

        this.x = x;
        this.y = y;
    }
}
