package ObjectClassCompareOfObjects;

public class Main {

    public static void main(String[] args) {

        Pkt p = new Pkt(4, 10);     //nowy adres
        Pkt p1 = new Pkt(4, 10);    //nowy adres

        System.out.println("p.getClass() " + p.getClass());
        System.out.println("-------------------------------------------------");

        if (p == p1){
            System.out.println("valeuse are the same");
        } else {
            System.out.println("valeuse aren't the same because addresses are diffrent");
        }


        if (p.equals(p1)){

            System.out.println("by the equals the values are the same ");
        } else {
            System.out.println("by the equals the values aren't the same ");
        }

        Object[] pkt = new Pkt[4];

        pkt[0] = new Pkt(2,4);
        pkt[1] = new Pkt(1,7);
        pkt[2] = new Pkt(12,3);
        pkt[3] = new Pkt(5,9);

        for (int i = 0; i < pkt.length; i++){
            System.out.println("this is toString method" + pkt[i]);
        }
    }
}
