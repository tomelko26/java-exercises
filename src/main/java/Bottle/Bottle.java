package Bottle;

public class Bottle {

    static int howManyBottles = 50;
    private double howManyLiters;
    private double maxCapacity;

    public Bottle(double howManyLiters, double maxCapacity) {
        this.howManyLiters = howManyLiters;
        this.maxCapacity = maxCapacity;
    }


    public double getHowManyLiters() {
        return howManyLiters;
    }

    public void setHowManyLiters(double howManyLiters) {
        this.howManyLiters = howManyLiters;
    }

    public double getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(double maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public static int getHowManyBottles() {
        return howManyBottles;
    }

    double pour (double howMany, double maxV){
        if(howManyLiters + howMany <= maxV){
            howManyLiters += howMany;
            return 0;
        } else {
            double rest = howMany - (maxV - howManyLiters);
            howManyLiters = maxV;
            System.out.printf("zalałem pod korek, no więcej nie upchniesz -  " +  maxV + " ");
            return rest;
        }
    }

    boolean pourOut (double howMany) {
        if (howMany <= howManyLiters) {
            howManyLiters -= howMany;
            return true;
        } else {
            return false;
        }
    }

    void transfer (double howMany, Bottle whichOne, double maxV){
       if (this.pourOut(howMany)){
           double rest = whichOne.pour(howMany, maxV);
           if (rest != 0){
               this.pour(rest, this.getHowManyLiters());
           }
       } else {
           System.out.printf("uważaj! bo Ci się uleje");
       }

    }



    public static void main(String[] args) {
        Bottle[] bottle = new Bottle[howManyBottles];

        for (int i =0; i < howManyBottles; i++){
            bottle[i] = new Bottle(5,100);
        }

        bottle[14].setHowManyLiters(5);
        bottle[14].setMaxCapacity(100);
        bottle[32].setHowManyLiters(1);
        bottle[32].setMaxCapacity(50);
        bottle[41].setHowManyLiters(2);
        bottle[41].setMaxCapacity(50);

        
        
        bottle[14].pour(80, bottle[0].getHowManyLiters());
        System.out.println(bottle[14].getHowManyLiters() + " to butelkac 14");

        bottle[14].pourOut(1);
        System.out.println(bottle[14].getHowManyLiters() + " to butelkac 14");

        bottle[14].transfer(10, bottle[41], bottle[41].getHowManyLiters());
        System.out.println(bottle[14].getHowManyLiters()  + " to butelka 14" );

        bottle[41].pour(0, bottle[41].getHowManyLiters());
        System.out.println(bottle[41].getHowManyLiters()  + " to butelkac 41");

        bottle[32].pourOut(3);
        System.out.println(bottle[32].getHowManyLiters()  + " to butelka 32");

        bottle[32].pourOut(50);
        System.out.println(bottle[32].getHowManyLiters() + "to butelka 32" );

        
    }

}


