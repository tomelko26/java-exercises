package Bottle;

public class Bottlle2 {
    private double howManyLiters;

    Bottlle2(double howManyLiters)
    {
        this.howManyLiters = howManyLiters;
    }

    double getHowManyLiters()
    {
        return howManyLiters;
    }
    void pourIn(double amount)
    {
        this.howManyLiters += amount;
    }
    void pourOut(double amount) throws ToSmallAmountOfWater
    {
        if (amount < howManyLiters)
            this.howManyLiters -= amount;
        else
            throw new ToSmallAmountOfWater("Za mało wody");

    }

    void transfer(double amount, Bottlle2 gdziePrzelac) throws ToSmallAmountOfWater
    {
        this.pourOut(amount);
        gdziePrzelac.pourIn(amount);

    }


    public static void main(String[] args)
    {
        Bottlle2 k = new Bottlle2(5);
        Bottlle2 k2 = new Bottlle2(10);

        try
        {
            k.transfer(20, k2);
        }
        catch(ToSmallAmountOfWater ex)
        {
            System.out.println(ex.getMessage());
        }


        int a = 10;

        try
        {
            System.out.println(5/0);
            if (a == 10)
                throw new NaszWyjatek("a jest rowne 10, grzeszysz");
        }
        catch(NaszWyjatek ex)
        {
            System.out.println(ex.getMessage());
        }
        catch(ArithmeticException ex)
        {
            System.out.println(ex.getMessage());
        }

        try
        {
            System.out.println(5/1);



        }
        catch(Exception ex)
        {
            System.out.println("Powstał wyjątek: " + ex.getMessage());
        }
        finally
        {
            System.out.println("finally -  COS CO ZAWSZE ZOSTANIE WYWOLANE");
        }

        System.out.println("cokolwiek");

        /*
            try
            {
                //INSTRUKCJE KTORE POTENCJALNIE MOGA SPOWODOWAC BLAD
            }
            catch(NazwaZwrconegoWyjatku nazwazmiennej)
            {
                //INSTRUKCJE OBSLUGUJACE ZLAPANY WYJATEK
            }

        */

    }



}
