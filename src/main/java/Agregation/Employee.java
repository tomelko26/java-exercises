package Agregation;

public class Employee {

    String name;
    Address address;

    public Employee(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    @Override
    public String toString() {
        return this.name + ": " + address.street + " " + address.houseNr;
    }
}
