package Agregation;

public class Address {

    String street;
    int houseNr;

    public Address(String street, int houseNr) {
        this.street = street;
        this.houseNr = houseNr;
    }
}
