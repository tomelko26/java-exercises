package NestedClasses;

public class One {

    public One() {
        System.out.println("outside class The One");
    }

    class Second{

        public Second() {
            System.out.println("inside/ nested class Second");
        }

        void somthing(){

            test = 5;
        }

        private int temp;

    }

    static class Third{

        Third(){
            System.out.println("inside/ nested class Third");
        }

    }


    void somthing2(){

//        temp = 2;
        Second obj = new Second();
        obj.temp = 12;
    }

    private int test;

}
