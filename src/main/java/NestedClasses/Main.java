package NestedClasses;

public class Main {

    public static void main(String[] args) {

        One o = new One();

        One.Second t = o.new Second();

        One.Third t2 = new One.Third();

    }

}
