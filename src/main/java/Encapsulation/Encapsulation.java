package Encapsulation;

public class Encapsulation {
    /*
    Enkapsulacja- hermetyzacja- czyli pakowanie właściwości w taki sposób
                                aby nie było do nich bezpośredniego dostepu
                                z innych klas
     */
    public static void main(String[] args) {

        BankAccount savings = new BankAccount();
        savings.setSaldo(6000);
        System.out.println(savings.getSaldo());

        savings.payoff(10000000);
        System.out.println(savings.getSaldo());

    }

}

class BankAccount {

    public BankAccount(){
        saldo = 1000;
    }

    private int saldo;

    int getSaldo(){
        return saldo;
    }

    boolean setSaldo (int saldo){
        /*
        WARUNKI !!!!
         */

        this.saldo = saldo;
        return true;
    }

    boolean payoff (int howMuch) {
        if (saldo < howMuch) {
            System.out.println("U don t have fu****g money!");
        } else {
            setSaldo(saldo - howMuch);
            saldo -= howMuch;
            System.out.printf("Take this dirty cash of yours!");
        }
        return true;
    }

    boolean payment (int howMuch){
        setSaldo(saldo + howMuch);
        return true;
    }
}