package Methods;

public class Metody {

    public static void main(String[] args) {

        Test a = new Test();
        a.write("Nefi","Kotka");

        Test a2 = new Test();
        a2.write("Nati", "TeżKoti");

        int result= a.add(10,225);
        System.out.println(result);

        double result1 = a.division(5,7);
        System.out.println(result1);
    }

}

class Test{

    void write (String name, String surname){
        System.out.println("imie: "+ name);
        System.out.println("nazwisko: " + surname);
        System.out.println();
    }

    int add(int a, int b){
        return  a+b;
    }
    double add(double a, double b){ //przeciążenie metody add
        return  a+b;
    }

    double division (double a, double b){
        if (b==0){
            return 0;
        }

        return a / b;
    }
}